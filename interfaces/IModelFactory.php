<?php

/**
 *
 * @author pabhoz
 */
interface IModelFactory {
    
    public function newBrand($id, $name, $logo): Brand;
    public function newCategory($id,$name,$parent): Category;
    public function newClient(): Client;
    public function newMenu($id,$name,$rol): Menu;
    public function newMenuItem($id,$name,$url,$parent): MenuItem;
    public function newProduct(): Product;
    public function newProvider(): Provider;
    public function newReceipt(): Receipt;
    public function newRol(): Rol;
    public function newShoppingCar(): ShoppingCar;
    public function newSubcategory(): Subcategory;
    public function newUser($id, $username, $password, $email, $rol): User;
}
