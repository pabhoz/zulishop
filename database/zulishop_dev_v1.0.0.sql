-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema zulishop
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `zulishop` ;

-- -----------------------------------------------------
-- Schema zulishop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zulishop` DEFAULT CHARACTER SET utf8 ;
USE `zulishop` ;

-- -----------------------------------------------------
-- Table `zulishop`.`Client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Client` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `email_UNIQUE` ON `zulishop`.`Client` (`email` ASC);

CREATE UNIQUE INDEX `username_UNIQUE` ON `zulishop`.`Client` (`username` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Rol`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Rol` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Rol` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zulishop`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`User` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `rol` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_User_Rol`
    FOREIGN KEY (`rol`)
    REFERENCES `zulishop`.`Rol` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `username_UNIQUE` ON `zulishop`.`User` (`username` ASC);

CREATE UNIQUE INDEX `email_UNIQUE` ON `zulishop`.`User` (`email` ASC);

CREATE INDEX `fk_User_Rol_idx` ON `zulishop`.`User` (`rol` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Category` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `parent` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Category_Category1`
    FOREIGN KEY (`parent`)
    REFERENCES `zulishop`.`Category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `zulishop`.`Category` (`name` ASC);

CREATE INDEX `fk_Category_Category1_idx` ON `zulishop`.`Category` (`parent` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Subcategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Subcategory` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Subcategory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `category` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Subcategory_Category1`
    FOREIGN KEY (`category`)
    REFERENCES `zulishop`.`Category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `zulishop`.`Subcategory` (`name` ASC);

CREATE INDEX `fk_Subcategory_Category1_idx` ON `zulishop`.`Subcategory` (`category` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Brand`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Brand` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Brand` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `logo` VARCHAR(128) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zulishop`.`Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Product` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` DECIMAL(15,2) NOT NULL DEFAULT 0,
  `quantity` VARCHAR(45) NOT NULL DEFAULT 0,
  `brand` INT NOT NULL,
  `disscount` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Product_Brand1`
    FOREIGN KEY (`brand`)
    REFERENCES `zulishop`.`Brand` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_Brand1_idx` ON `zulishop`.`Product` (`brand` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Provider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Provider` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Provider` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `tel` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zulishop`.`Product_x_Provider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Product_x_Provider` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Product_x_Provider` (
  `Product_id` INT NOT NULL,
  `Provider_id` INT NOT NULL,
  PRIMARY KEY (`Product_id`, `Provider_id`),
  CONSTRAINT `fk_Product_has_Provider_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishop`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_has_Provider_Provider1`
    FOREIGN KEY (`Provider_id`)
    REFERENCES `zulishop`.`Provider` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_has_Provider_Provider1_idx` ON `zulishop`.`Product_x_Provider` (`Provider_id` ASC);

CREATE INDEX `fk_Product_has_Provider_Product1_idx` ON `zulishop`.`Product_x_Provider` (`Product_id` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Product_x_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Product_x_Category` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Product_x_Category` (
  `Product_id` INT NOT NULL,
  `Category_id` INT NOT NULL,
  PRIMARY KEY (`Product_id`, `Category_id`),
  CONSTRAINT `fk_Product_has_Category_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishop`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_has_Category_Category1`
    FOREIGN KEY (`Category_id`)
    REFERENCES `zulishop`.`Category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_has_Category_Category1_idx` ON `zulishop`.`Product_x_Category` (`Category_id` ASC);

CREATE INDEX `fk_Product_has_Category_Product1_idx` ON `zulishop`.`Product_x_Category` (`Product_id` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`ShoppingCar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`ShoppingCar` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`ShoppingCar` (
  `Client_id` INT NOT NULL,
  `Product_id` INT NOT NULL,
  `quantity` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`Client_id`, `Product_id`),
  CONSTRAINT `fk_Client_has_Product_Client1`
    FOREIGN KEY (`Client_id`)
    REFERENCES `zulishop`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Client_has_Product_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishop`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Client_has_Product_Product1_idx` ON `zulishop`.`ShoppingCar` (`Product_id` ASC);

CREATE INDEX `fk_Client_has_Product_Client1_idx` ON `zulishop`.`ShoppingCar` (`Client_id` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Receipt` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Receipt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` TIMESTAMP NOT NULL,
  `client` INT NOT NULL,
  `total` DECIMAL(15,2) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Receipt_Client1`
    FOREIGN KEY (`client`)
    REFERENCES `zulishop`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Receipt_Client1_idx` ON `zulishop`.`Receipt` (`client` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Product_x_Receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Product_x_Receipt` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Product_x_Receipt` (
  `Product_id` INT NOT NULL,
  `Receipt_id` INT NOT NULL,
  `quantity` INT NOT NULL,
  `price` DECIMAL(15,2) NOT NULL COMMENT 'Unit price at time of sale',
  PRIMARY KEY (`Product_id`, `Receipt_id`),
  CONSTRAINT `fk_Product_has_Receipt_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishop`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_has_Receipt_Receipt1`
    FOREIGN KEY (`Receipt_id`)
    REFERENCES `zulishop`.`Receipt` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_has_Receipt_Receipt1_idx` ON `zulishop`.`Product_x_Receipt` (`Receipt_id` ASC);

CREATE INDEX `fk_Product_has_Receipt_Product1_idx` ON `zulishop`.`Product_x_Receipt` (`Product_id` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Menu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Menu` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `rol` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Menu_Rol1`
    FOREIGN KEY (`rol`)
    REFERENCES `zulishop`.`Rol` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Menu_Rol1_idx` ON `zulishop`.`Menu` (`rol` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`MenuItem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`MenuItem` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`MenuItem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `url` VARCHAR(256) NOT NULL,
  `parent` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_MenuItem_MenuItem1`
    FOREIGN KEY (`parent`)
    REFERENCES `zulishop`.`MenuItem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_MenuItem_MenuItem1_idx` ON `zulishop`.`MenuItem` (`parent` ASC);


-- -----------------------------------------------------
-- Table `zulishop`.`Menu_x_Item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishop`.`Menu_x_Item` ;

CREATE TABLE IF NOT EXISTS `zulishop`.`Menu_x_Item` (
  `Menu_id` INT NOT NULL,
  `MenuItem_id` INT NOT NULL,
  PRIMARY KEY (`Menu_id`, `MenuItem_id`),
  CONSTRAINT `fk_Menu_has_MenuItem_Menu1`
    FOREIGN KEY (`Menu_id`)
    REFERENCES `zulishop`.`Menu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Menu_has_MenuItem_MenuItem1`
    FOREIGN KEY (`MenuItem_id`)
    REFERENCES `zulishop`.`MenuItem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Menu_has_MenuItem_MenuItem1_idx` ON `zulishop`.`Menu_x_Item` (`MenuItem_id` ASC);

CREATE INDEX `fk_Menu_has_MenuItem_Menu1_idx` ON `zulishop`.`Menu_x_Item` (`Menu_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
