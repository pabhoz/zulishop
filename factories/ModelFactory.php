<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelFactory
 *
 * @author pabhoz
 */
class ModelFactory implements IModelFactory{
    
    
    public function newBrand($id, $name, $logo): \Brand {
        
    }

    public function newCategory($id, $name, $parent): \Category {
        
    }

    public function newClient(): \Client {
        
    }

    public function newMenu($id, $name, $rol): \Menu {
        return new Menu($id, $name, $rol);
    }

    public function newMenuItem($id, $name, $url, $parent): \MenuItem {
        return new MenuItem($id, $name, $url, $parent);
    }

    public function newProduct(): \Product {
        
    }

    public function newProvider(): \Provider {
        
    }

    public function newReceipt(): \Receipt {
        
    }

    public function newRol(): \Rol {
        
    }

    public function newShoppingCar(): \ShoppingCar {
        
    }

    public function newSubcategory(): \Subcategory {
        
    }

    public function newUser($id, $username, $password, $email, $rol): \User {
        return new User($id, $username, $password, $email, $rol);
    }

}
